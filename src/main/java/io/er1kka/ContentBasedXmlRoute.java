package io.er1kka;

import org.apache.camel.Predicate;
import org.apache.camel.builder.PredicateBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.language.xpath.XPathBuilder;
import org.springframework.stereotype.Component;


@Component
public class ContentBasedXmlRoute extends RouteBuilder {

    public static final Predicate honda = PredicateBuilder.and(XPathBuilder.xpath("/order[(@customer) = 'honda']"));
    public static final Predicate motor = PredicateBuilder.and(XPathBuilder.xpath("/order[(@name) = 'motor']"));

    @Override
    public void configure() throws Exception {
        from("file:src/data/in?noop=true")
                .routeId("predicates")
                .choice()
                    .when(honda).to("log:honda")
                    .when(motor).to("log:motor")
                    .otherwise().to("log:unknown");
    }
}
