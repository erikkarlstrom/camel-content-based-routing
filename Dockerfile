FROM adoptopenjdk:11-jre-hotspot
COPY target/*jar /opt/app/camel-content-based-1.0-SNAPSHOT.jar
CMD ["java", "-jar", "/opt/app/camel-content-based-1.0-SNAPSHOT.jar"]